#pragma once
#include"FileStream.h"
#include"OutStream.h"


class Logger
{
private:
	OutStream _screen;
	FileStream _file;
	bool _logToScreen;
public:
	Logger(const char *filename, bool logToScreen);
	~Logger();

	void print(const char *msg);
};
