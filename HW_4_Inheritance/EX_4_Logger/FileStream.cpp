#define _CRT_SECURE_NO_WARNINGS

#include"FileStream.h"

//the contructor open the file
FileStream::FileStream(const char* nameOfFile)
{
	this->_dstFile = fopen(nameOfFile, "w");
}
//the destructor close the file
FileStream::~FileStream()
{
	fclose(this->_dstFile);
}