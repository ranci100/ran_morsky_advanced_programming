#define _CRT_SECURE_NO_WARNINGS

#include"Logger.h"

//the contructor open the file
Logger::Logger(const char* filename, bool logToScreen):_file(filename)
{
	this->_logToScreen = logToScreen;
}
//the destructor close the file
Logger::~Logger()
{
}

void Logger::print(const char* msg)
{
	if (this->_logToScreen)
	{
		this->_screen << msg;
	}
	this->_file << msg;
}