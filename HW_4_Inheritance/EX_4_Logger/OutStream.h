#pragma once
#include <stdio.h>


class OutStream
{
protected:
	//Fields
	FILE* _dstFile;


public:
	//constructors
	OutStream();
	//destructor
	~OutStream();


	OutStream& operator<<(const char* str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE* file));

};

// the tunction do enter in the file
void endline(FILE* file);

