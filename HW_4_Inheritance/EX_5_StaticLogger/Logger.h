#pragma once
#include"FileStream.h"
#include"OutStream.h"


class Logger
{
private:
	//fildes:
	OutStream _screen;
	FileStream _file;
	bool _logToScreen;
	static unsigned int _printCounter;
public:
	//constructor who init the file and choose the dst output
	Logger(const char* filename, bool logToScreen);
	~Logger();
	//the function print msg to the dst
	void print(const char* msg);
};
