#define _CRT_SECURE_NO_WARNINGS

#include"Logger.h"

unsigned int Logger::_printCounter = 1;

//the contructor open the file
Logger::Logger(const char* filename, bool logToScreen) :_file(filename)
{
	this->_logToScreen = logToScreen;
}
//the destructor close the file
Logger::~Logger()
{
}
//the function print msg to the dst
void Logger::print(const char* msg)
{
	if (this->_logToScreen)
	{
		this->_screen << this->_printCounter;
		this->_screen << msg;
	}
	else {
		this->_file << this->_printCounter;
		this->_file << msg;
	}
	this->_printCounter++;
}