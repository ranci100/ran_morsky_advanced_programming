#pragma once
#include <stdio.h>
#include "OutStream.h"


class OutStreamEncrypted : public OutStream
{
private:
	int _hist;
	//the function change the letter by the hist
	char* changeLetters(int hist, const char* str);
public:
	//constructors

	OutStreamEncrypted(int hist);
	//destructor
	~OutStreamEncrypted();

	//the function is init the operator << to get string
	OutStreamEncrypted& operator<<(const char* str);
	//the function is init the operator << to get pointer of function and print new line
	OutStreamEncrypted& operator<<(void(*pf)(FILE* file));



};
