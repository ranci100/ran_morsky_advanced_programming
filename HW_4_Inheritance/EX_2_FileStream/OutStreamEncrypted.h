#pragma once
#include <stdio.h>
#include "OutStream.h"


class OutStreamEncrypted : public OutStream
{
private:
	int _hist;
	char* changeLetters(int hist,const char* str);
public:
	//constructors

	OutStreamEncrypted(int hist);
	//destructor
	~OutStreamEncrypted();

	OutStreamEncrypted& operator<<(const char* str);
	OutStreamEncrypted& operator<<(void(*pf)(FILE* file));



};



