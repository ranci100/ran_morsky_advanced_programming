#define _CRT_SECURE_NO_WARNINGS

#include "OutStreamEncrypted.h"
#include <string.h>

#define BIGGEST_ASCCI_VALUE 126
#define SMALLEST_ASCCI_VALUE 32

OutStreamEncrypted::OutStreamEncrypted(int hist)
{
	this->_hist = hist;
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char* str)
{
	OutStream print;
	char* encryptedString = NULL;
	encryptedString = changeLetters(this->_hist, str);
	
	
	print << encryptedString;

	return *this;
}


OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE* file))
{
	OutStream print;

	print << endline;
	return *this;
}

char* OutStreamEncrypted::changeLetters(int hist,const char* str)
{
	int i = 0;
	int spacer = 0;
	char* newStr = new char[strlen(str)];

	newStr[strlen(str)] = 0;
	while (str[i] != 0)
	{
		spacer = (int)str[i];
		if (str[i] <= BIGGEST_ASCCI_VALUE && str[i] >= SMALLEST_ASCCI_VALUE)
		{
			spacer = ((int)str[i] + hist);			
			if (spacer > BIGGEST_ASCCI_VALUE)
			{
				spacer = spacer % (BIGGEST_ASCCI_VALUE - SMALLEST_ASCCI_VALUE)-1;
			}
		}
		newStr[i] = (char)spacer;
		i++;
	}
	
	return newStr;
}