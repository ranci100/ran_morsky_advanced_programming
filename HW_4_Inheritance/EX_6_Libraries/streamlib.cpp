#define _CRT_SECURE_NO_WARNINGS

#include "streamlib.h"
#include <stdio.h>
#include <string.h>
using namespace msl;
//constructor that init the dstFile to the screen
OutStream::OutStream()
{
	this->_dstFile = stdout;
}
//destructor
OutStream::~OutStream()
{
}

/*
	this function get string and print the string to file
*/
OutStream& OutStream::operator<<(const char* str)
{
	fprintf(this->_dstFile, str);
	return *this;
}

/*
	this function get num and print the num to file
*/
OutStream& OutStream::operator<<(int num)
{
	fprintf(this->_dstFile, "%d", num);
	return *this;
}

/*
	this function get pointer to function ,call the function and transfer to the function a file pointer
*/
OutStream& OutStream::operator<<(void(*pf)(FILE* file))
{
	pf(this->_dstFile);
	return *this;
}

/*
	the function print to file \n
*/

void msl::endline(FILE* file)
{
	fprintf(file, "\n");
}
