#include <iostream>
#include "vector.h"

#define MIN_SIZE_OF_ARRAY 2
#define VECTOR_EMPTY -1
#define CAN_NOT_POP -9999

/*
	the function get begining size and init the vector
*/
Vector::Vector(int n)
{
	if (n < MIN_SIZE_OF_ARRAY)
	{
		n = 2;
	}
	this->_elements = new int[n];
	this->_resizeFactor = n;
	this->_capacity = n;
	this->_size = VECTOR_EMPTY;
}
/*
	the function free all of the allocated memory
*/
Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}

/*
	the function return the current size of the vector 
*/
int Vector::size() const
{
	if (empty())
	{
		return 0;
	}

	return this->_size+1;
}
/*
	the function return the current capacity of the vector
*/
int Vector::capacity() const
{
	return this->_capacity;
}

/*
	the function return the resize factor of the vector
*/
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}
/*
	the function return true if the vector is empty and false if not
*/
bool Vector::empty() const
{
	if (this->_size == VECTOR_EMPTY)
	{
		return true;
	}
	
	return false;
}
/*
	the function add value after the last number in the vector 
*/
void Vector::push_back(const int& val) 
{
	int i = 0;
	if (this->_size + 1 == this->_capacity)
	{
		int* spacerArray = new int[this->_capacity + this->_resizeFactor];
		
		for (i = 0; i < this->_size; i++)
		{
			spacerArray[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = spacerArray;
		this->_capacity += this->_resizeFactor;
	}
	else
	{
		this->_size++;
		this->_elements[this->_size] = val;
	}
}
/*
	the function delete and return the last value in the vector 
*/
int Vector::pop_back()
{
	if (empty())
	{
		std::cout << "error: pop from empty vector0" << std::endl;
		return CAN_NOT_POP;
	}
	else
	{
		this->_size--;
		return this->_elements[this->_size + 1];
	}
}
/*
	the function the function change the capacity of the vector according to resize factor
*/
void Vector::reserve(int n)
{
	int i = 0;

	if (this->_capacity < n)
	{
		if (n % this->_resizeFactor == 0)
		{
			this->_capacity = n;
			int* spacerArray = new int[this->_capacity];
			for (i = 0; i < this->_size; i++)
			{
				spacerArray[i] = this->_elements[i];
			}
		}
		else
		{
			this->_capacity = n + (this->_resizeFactor - n % this->_resizeFactor);
			int* spacerArray = new int[this->_capacity];
			for (i = 0; i < this->_size; i++)
			{
				spacerArray[i] = this->_elements[i];
			}
		}
		
	}
}

/*
	the function change the size of the vector according to the parameter n and if the parameter bigger than the capacity the function call the function reserve
*/
void Vector::resize(int n)
{
	if (n > this->_capacity)
	{
		reserve(n);
	}
	else if(n >= 0)
	{
		this->_size = n;
	}
	else
	{
		std::cout << "can't go to this location try with positiv number!" << std::endl;
	}
}
/*
	the function change all off the vector values to the value in the parameter
*/
void Vector::assign(int val)
{
	int i = 0;
	
	for (i = 0; i <= this->_size; i++)
	{
		this->_elements[i] = val;
	}
}
/*
	the function change the size of the vector according to the parameter n and if the parameter bigger than the capacity the function call the function reserve
	if n is bigger than size then the function will change the new values to val
*/
void Vector::resize(int n,const int &val)
{
	int i = 0;
	if (n > this->_capacity)
	{
		reserve(n);
		for ( i = this->_size + 1; i < n; i++)
		{
			this->_elements[i] = val;
		}
	}
	else
	{
		if (n > this->_size)
		{
			for ( i = this->_size + 1; i < n; i++)
			{
				this->_elements[i] = val;
			}
		}
	}
	this->_size = n-1;
}
/*
	the function the function get vector and make it equa to this vector
*/
Vector::Vector(const Vector& other)
{
	*this = other;
}

/*
	the function set the operator = to set all of the privete values according to the values that the function get from the parameter other
*/
Vector& Vector::operator=(const Vector& other)
{
	int i = 0;

	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}

	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;

	delete[] this->_elements;
	
	for (size_t i = 0; i <= other._size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this; 
}

/*
	the function the function return the value in the element number n in the vector
*/
int& Vector::operator[](int n) const
{
	if (n > this->_size || n < 0)
	{
		std::cout << "There is no value in: " << n << std::endl;
		return this->_elements[0];
	}

	return this->_elements[n];
}
/*
	the function sub all of the elements value vector from this vector
*/
Vector& Vector::operator-=(const Vector& other)
{
	int i = 0;
	if (this->_size > other._size)
	{
		i = other._size;
	}
	else
	{
		i = this->_size;
	}
	for ( i ; i > 0; i++)
	{
		this->_elements[i] -= other._elements[i];
	}
	return *this;
}
/*
	the function add all of the elements value vector to this vector
*/
Vector& Vector::operator+=(const Vector& other)
{
	int i = 0;
	if (this->_size > other._size)
	{
		i = other._size;
	}
	else
	{
		i = this->_size;
	}
	for (i; i > 0; i++)
	{
		this->_elements[i] += other._elements[i];
	}
	return *this;
}