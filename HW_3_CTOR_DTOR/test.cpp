#include <iostream>
#include "vector.h"

#define MIN_SIZE_OF_ARRAY 2

int main(void)
{
	int choose = 1;
	int i = 8;
	Vector vector(4);
	Vector v2(0);

	while (choose != 0)
	{
		std::cout << "please choose function: " << std::endl;
		std::cin >> choose;
		if (choose == 1)
		{
			std::cout << "empty?" << std::endl;
			if (vector.empty())
			{
				std::cout << "yes" << std::endl;
			}
			else
			{
				std::cout << "no" << std::endl;
			}
		}
		if (choose == 2)
		{
			std::cout << "size:" << std::endl;
			std::cout << vector.size() << std::endl;
		}
		else if (choose == 3)
		{
			std::cout << "capacity:" << std::endl;
			std::cout << vector.capacity() << std::endl;
		}
		else if (choose == 4)
		{
			std::cout << "resize factor:" << std::endl;
			std::cout << vector.resizeFactor() << std::endl;
		}
		else if (choose == 5)
		{
			std::cout << "pushing back! " << std::endl;
			std::cin >> i;
			vector.push_back(i);
		}
		else if (choose == 6)
		{
			std::cout << "poped back: " << std::endl;
			std::cout << vector.pop_back() << std::endl;
		}
		else if (choose == 7)
		{
			std::cout << "capacity is: " << std::endl;
			vector.reserve(30);
			std::cout << vector.capacity() << std::endl;
		}
		else if (choose == 8)
		{
			std::cout << "resize to: " << std::endl;
			vector.resize(7,i);
			std::cout << vector.size() << std::endl;
		}
		else if (choose == 9)
		{
			std::cout << "assign! " << std::endl;
			vector.assign(8);
		}
		else if(choose == 10)
		{
			v2 = vector;
			std::cout << "capacity of v2";
			std::cout << v2.capacity() << std::endl;
			std::cout << "size of v2";
			std::cout << v2.size() << std::endl;
			std::cout << "resizeFactor of v2";
			std::cout << v2.resizeFactor() << std::endl;
			std::cout << "empty of v2:";
			std::cout << v2.empty()<<std::endl;
			std::cout << v2[1] << std::endl;
		}
		else
		{
			std::cout << v2[1] << std::endl;
		}
		
	}
	
	
	
	
	
	return 0;
}