#include "Rectangle.h"



//the constructor init the points and the polygon
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name):Polygon(type,name),_leftTop(a)
{
	this->_length = length;
	this->_width = width;
	this->_points.push_back(this->_leftTop);
	Point b(this->_leftTop.getX() + width, this->_leftTop.getY() + length);
	this->_points.push_back(b);
}

myShapes::Rectangle::~Rectangle()
{

}
//the function draw to the screen shape
void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}
//the function erase from the screen the shape
void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}
//the function return the area of the shape
double myShapes::Rectangle::getArea() const
{
	return this->_length* this->_width;
}
//the function return the perimeter of the shape
double myShapes::Rectangle::getPerimeter() const
{
	return this->_length + this->_length + this->_width + this->_width;
}
//the function move the shape
void myShapes::Rectangle::move(const Point& other)
{
	this->_leftTop += other;
	Point rightBottom(this->_leftTop.getX() + this->_width, this->_leftTop.getY() + this->_length);
	this->_points.clear();
	this->_points.push_back(this->_leftTop);
	this->_points.push_back(rightBottom);
}
