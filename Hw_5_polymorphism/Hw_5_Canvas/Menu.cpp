#include "Menu.h"



Menu::Menu()
{

}

Menu::~Menu()
{
	int i = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		delete this->_shapes[i];
	}
}



void Menu::addShape()
{
	int choose = -1; //smaller than min choice
	while (choose > ADD_RECTANGLE || choose < ADD_CIRCLE)
	{
		system("cls");
		std::cout << "Enter 0 to add a circle. " << std::endl;
		std::cout << "Enter 1 to add an arrow." << std::endl;
		std::cout << "Enter 2 to add a triangle." << std::endl;
		std::cout << "Enter 3 to add a rectangle." << std::endl;
		std::cin >> choose;
	}
	if (choose == ADD_CIRCLE)
	{
		this->_shapes.push_back(this->getCircle());
		this->_shapes[this->_shapes.size() - 1]->draw((this->_canvas));
	}     
	else if (choose == ADD_ARROW)
	{
		this->_shapes.push_back(this->getArrow());
		this->_shapes[this->_shapes.size()-1]->draw((this->_canvas));
	}
	else if (choose == ADD_TRIANGLE)
	{
		if (getTriangle())
		{
			this->_shapes[this->_shapes.size() - 1]->draw((this->_canvas));
		}
		
	}
	else if (choose == ADD_RECTANGLE)
	{
		if (getRectagnle())
		{
			this->_shapes[this->_shapes.size() - 1]->draw((this->_canvas));
		}
	}
}

//get data for new circle from user
Shape* Menu::getCircle()
{
	int x = 0;
	int y = 0;
	double radius = 0;
	std::string name;

	//get center point
	std::cout << "Please enter X:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter Y:" << std::endl;
	std::cin >> y;
	std::cout << "Please enter radius:" << std::endl;
	std::cin >> radius;
	std::cout << "Please enter the name of the shape:" << std::endl;
	std::cin >> name;

	//check if radius bigger than zero
	radius = std::abs(radius);
	
	Point center(x, y);
	Circle* newCircle = new Circle(center, radius, "Circle", name);

	return newCircle;
}

bool Menu::getTriangle()
{
	int x = 0;
	int y = 0;
	std::string name;
	//get data for the 3 points
	std::cout << "Enter the X of point number: 1" << std::endl;
	std::cin >> x; 
	std::cout << "Enter the Y of point number: 1" << std::endl;
	std::cin >> y;
	Point a(x,y);
	std::cout << "Enter the X of point number: 2" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of point number: 2" << std::endl;
	std::cin >> y;
	Point b(x,y);
	std::cout << "Enter the X of point number: 3" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of point number: 3" << std::endl;
	std::cin >> y;
	Point c(x,y); 

	//get name of triangle 
	std::cout << "Enter the name of the shape :" << std::endl;
	std::cin >> name;

	//check if the points are on the same line
	if (a.getX() == b.getX() && b.getX() == c.getX() || a.getY() == b.getY() && b.getY() == c.getY() || a.getX() == a.getY() && b.getX() == b.getY() && c.getX() == c.getY())
	{
		getchar();
		std::cout << "The points are on the same line!";
		getchar();
		return false;
		
	}
	else
	{
		Triangle* newTriangle = new Triangle(a, b, c, "Triangle", name);
		this->_shapes.push_back(newTriangle);
		return true;
	}
}

bool Menu::getRectagnle()
{
	int x = 0;
	int y = 0;
	int width = 0;
	int length = 0;
	std::string name;

	//get data for left top point, width and length of the rectangle
	std::cout << "Enter the X of the to left corner:" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of the to left corner:" << std::endl;
	std::cin >> y;
	Point leftTop(x, y);

	std::cout << "Please enter the length of the shape:" << std::endl;
	std::cin >> length;
	std::cout << "Please enter the width of the shape:" << std::endl;
	std::cin >> width;

	// turn the value to positive if negetive
	length = std::abs(length);
	width = std::abs(width);

	//get name of rectangle
	std::cout << "Enter the name of the shape :" << std::endl;
	std::cin >> name;
	if (length == 0 || width == 0)
	{
		getchar();
		std::cout << "Length or Width can't be 0." << std::endl;
		getchar();
		return false;
	}
	else
	{
		myShapes::Rectangle* newRectangle = new myShapes::Rectangle(leftTop, length, width, "Rectangle", name);
		this->_shapes.push_back(newRectangle);
		return true;
	}
}

Shape* Menu::getArrow()
{
	int x = 0;
	int y = 0;
	std::string name;
	//get data for left top point, width and length of the rectangle
	std::cout << "Enter the X of point number: 1" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of point number: 1" << std::endl;
	std::cin >> y;
	Point a(x, y);
	std::cout << "Enter the X of point number: 2" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of point number: 2" << std::endl;
	std::cin >> y;
	Point b(x, y);

	//get name of arrow
	std::cout << "Enter the name of the shape :" << std::endl;
	std::cin >> name;

	Arrow* newArrow = new Arrow(a, b, "Arrow", name);
	return newArrow;
}

void Menu::modifyAndInformation()
{
	int i = 0;
	int chooseShape = -1;//smaller than min num of shape
	int choose = -1;//smaller than min choice
	if (this->_shapes.size())
	{
		
		while (chooseShape < 0 || chooseShape >= this->_shapes.size())
		{
			system("cls");
			for (i = 0; i < this->_shapes.size(); i++)
			{
				std::cout << "Enter " << i << " for " << this->_shapes[i]->getName() << " (" << this->_shapes[i]->getType() << ")" << std::endl;
			}
			std::cin >> chooseShape;
		}
		while (choose < 0 || choose > REMOVE_SHAPE)
		{
			std::cout << "Enter 0 to move the shape" << std::endl;
			std::cout << "Enter 1 to get its details" << std::endl;
			std::cout << "Enter 2 to remove the shape." << std::endl;
			std::cin >> choose;
		}
		if (choose == MOVE_SHAPE)
		{
			move(chooseShape);
		}
		else if (choose == GET_DETAILS)
		{
			std::cout << this->_shapes[chooseShape]->getType() << "		" << this->_shapes[chooseShape]->getName() << "		" << this->_shapes[chooseShape]->getArea() << "		" << this->_shapes[chooseShape]->getPerimeter() << std::endl;
			getchar();
			std::cout << "press enter to continue";
			getchar();
			
		}
		else
		{
			removeShape(chooseShape);
		}
	}
}

void Menu::move(int chooseShape)
{
	int x = 0;
	int y = 0;

	system("cls");
	std::cout << "Please enter the X moving scale:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the Y moving scale:" << std::endl;
	std::cin >> y;

	Point moveTo(x, y);
	this->_shapes[chooseShape]->clearDraw(this->_canvas);
	this->_shapes[chooseShape]->move(moveTo);
	this->_shapes[chooseShape]->draw(this->_canvas);
}

void Menu::removeShape(int chooseShape)
{
	int i = 0;
	this->_shapes[chooseShape]->clearDraw(this->_canvas);
	this->_shapes.erase(this->_shapes.begin() + chooseShape);
	for (i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[i]->draw(this->_canvas);
	}
}

void Menu::deleteAllShapes()
{
	int i = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[i]->clearDraw(this->_canvas);
	}
	this->_shapes.clear();
}
