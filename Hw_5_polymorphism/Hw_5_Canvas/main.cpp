#include "Menu.h"

void printAllFunctions();

int main()
{
	Menu allProgram;
	
	int choose = 0;
	
	while (choose != EXIT)
	{
		printAllFunctions();
		std::cin >> choose;

		if (choose == ADD_SHAPE)
		{
			allProgram.addShape();
		}
		else if (choose == MODIFY_OR_INFORMASHION)
		{
			allProgram.modifyAndInformation();
		}
		else if (choose == DELETE_ALL_SHAPES)
		{
			allProgram.deleteAllShapes();
		}
	}

	return 0;
}

void printAllFunctions()
{
	system("cls");
	std::cout << "Enter 0 to add a new shape. " << std::endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
	std::cout << "Enter 2 to delete all of the shapes." << std::endl;
	std::cout << "Enter 3 to exit." << std::endl;
}