#include "Shape.h"

//the constructor init the shape
Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

Shape::~Shape()
{

}

//the function print the details of shape
void Shape::printDetails() const
{
	std::cout << this->_type << this->_name << "		" << this->getArea() << " 	 " << this->getPerimeter();
}

//the function return the type of the shape
std::string Shape::getType() const
{
	return this->_type;
}
//the function return the name of the shape
std::string Shape::getName() const
{
	return this->_name;
}