#include "Point.h"

#define POW 2

//the constructor init the point with x and y
Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

//the constructor init the point with other point
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::~Point()
{

}

//the operator add to the x and y of point, x and y of other point 
Point Point::operator+(const Point& other) const
{
	Point result(this->_x + other._x, this->_y + other._y);
	return result;
}

//the operator add to the point other point 
Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
	return* this;
}

//the function return the x of point
double Point::getX() const
{
	return this->_x;
}

//the function return the x of point
double Point::getY() const
{
	return this->_y;
}

//the function calculate and return the distance from one point to another
double Point::distance(const Point& other) const
{
	return sqrt(pow(other._x - this->_x,POW) + pow(other._y - this->_y, POW));
}