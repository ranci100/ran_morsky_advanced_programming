#include "Arrow.h"

//the constructor init the points and the shape
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name):_a(a),_b(b),Shape(name, type)
{
	this->_points.push_back(this->_a);
	this->_points.push_back(this->_b);
}

Arrow::~Arrow()
{

}

//the function draw to the screen shape
void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1] );
}
//the function erase from the screen the shape
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}

//the function return the area of the shape
double Arrow::getArea() const
{
	return 0;
}

//the function return the perimeter of the shape
double Arrow::getPerimeter() const
{
	return this->_a.distance(this->_b);
}

//the function move the shape
void Arrow::move(const Point& other)
{
	this->_a += other;
	this->_b += other;
	this->_points.clear();
	this->_points.push_back(this->_a);
	this->_points.push_back(this->_b);
}