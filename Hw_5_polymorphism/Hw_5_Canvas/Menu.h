#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>
#include "Triangle.h"
#include "Circle.h"
#include "Arrow.h"
#include "Rectangle.h"

#define ADD_CIRCLE 0
#define ADD_ARROW 1
#define ADD_TRIANGLE 2
#define ADD_RECTANGLE 3

#define ADD_SHAPE 0
#define MODIFY_OR_INFORMASHION 1
#define DELETE_ALL_SHAPES 2
#define EXIT 3

#define MOVE_SHAPE 0
#define GET_DETAILS 1
#define REMOVE_SHAPE 2



class Menu
{
public:

	Menu();
	~Menu();

	void addShape();
	void deleteAllShapes();
	void modifyAndInformation();

private: 
	std::vector<Shape*> _shapes;
	Canvas _canvas;
	Shape* getCircle();
	bool getTriangle();
	bool getRectagnle();
	Shape* getArrow();
	void move(int chooseShape);
	void removeShape(int chooseShape);
};


