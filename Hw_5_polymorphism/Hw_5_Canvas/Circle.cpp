#include "Circle.h"

//the function draw to the screen shape
void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}
//the function erase from the screen the shape
void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

//the constructor init the points and the shape
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name):Shape(name,type),_center(center)
{
	this->_radius = radius;
	
}

Circle::~Circle()
{

}

//the function return the center point of the circle
const Point& Circle::getCenter() const
{
	return this->_center;
}

//the function return the radius of the circle
double Circle::getRadius() const
{
	return this->_radius;
}
//the function return the area of the shape
double Circle::getArea() const
{
	return this->_radius * this->_radius * PI;
}
//the function return the perimeter of the shape
double Circle::getPerimeter() const
{
	return (this->_radius + this->_radius) * PI;
}
//the function move the shape
void Circle::move(const Point& other)
{
	this->_center += other;
}