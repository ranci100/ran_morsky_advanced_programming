#include "Triangle.h"

#define HALF_OF_SQUARE 0.5
//the constructor init the points and the polygon
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name):Polygon(type,name),_a(a),_b(b),_c(c)
{
	this->_points.push_back(this->_a);
	this->_points.push_back(this->_b);
	this->_points.push_back(this->_c);
}

Triangle::~Triangle()
{

}
//the function draw to the screen shape
void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}
//the function erase from the screen the shape
void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

//the function return the area of the shape
double Triangle::getArea() const
{
	return HALF_OF_SQUARE * std::abs(this->_a.getX() * (this->_c.getY() - this->_b.getY()) + this->_a.getX() * (this->_a.getY() - this->_c.getY()) + this->_a.getX() * (this->_b.getY() - this->_a.getY()));
}
//the function return the perimeter of the shape
double Triangle::getPerimeter() const
{
	return this->_a.distance(this->_b) + this->_b.distance(this->_c) + this->_c.distance(this->_a);
}
//the function move the shape
void Triangle::move(const Point& other)
{
	this->_points.clear();
	this->_points.push_back(this->_a += other);
	this->_points.push_back(this->_b += other);
	this->_points.push_back(this->_c += other);
	
}