#pragma once
#include "Manager.h"
#include "InputException.h"

#define SIGNIN 1
#define SIGNOUT 2
#define CONNECTED_USERS 3
#define EXIT 4

class Menu
{
public:
	Menu();
	~Menu();
	
	void addUser(std::set<std::string>& users, std::mutex& mtxOfUsers);
	int printAndChooseOption(int& choose);
	void removeUser(std::set<std::string>& users, std::mutex& mtxOfUsers);
	void printUsers(std::set<std::string>& users);


};

