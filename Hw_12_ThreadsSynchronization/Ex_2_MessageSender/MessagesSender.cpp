#include "MessagesSender.h"


MessagesSender::MessagesSender()
{
}

MessagesSender::~MessagesSender()
{

}

//The function read from the data file all of the data messages
void MessagesSender::getMessagesFromFile(std::queue<std::string>& messages, std::string dataFilePath)
{
	std::ifstream dataFile;
	std::string line;
	if (this->_mtxOfMessages.try_lock())
	{
		this->_mtxOfMessages.unlock();
	}
	dataFile.open(dataFilePath);// open file in the first time
	

	while (true)
	{
		
		std::this_thread::sleep_for(std::chrono::milliseconds(TIME_TO_SLEEP_BETWEEN_GET_MESSAGES)); //sleep for a while
		this->_mtxOfMessages.lock();
		
		while (std::getline(dataFile, line))
		{
			messages.push(line);
		}
		
		this->_mtxOfMessages.unlock();
		this->_cv.notify_all(); //free waiting of others threads
		
		dataFile.close();
		dataFile.open(dataFilePath, std::ifstream::out | std::ifstream::trunc);
	}
	
}

//The function open the data and output files and the thread that reads\writes from\to this file
void MessagesSender::callthreads(std::queue<std::string>& messages, std::string dataFilePath, std::string outputFilePath, std::set<std::string>& users)
{
	
	std::thread getMessagesThread(&MessagesSender::getMessagesFromFile, this, ref(messages),dataFilePath );
	std::thread sendMessagesThread(&MessagesSender::sendMessagesToAllUsers, this, ref(messages), ref(users),outputFilePath);
	getMessagesThread.detach();
	sendMessagesThread.detach();	
}

//The function read the message from the queue and send it to output file
void MessagesSender::sendMessagesToAllUsers(std::queue<std::string>& messages, std::set<std::string>& users, std::string outputFilePath)
{
	std::string currentMassege;
	int i = 0;
	std::set<std::string>::iterator it = users.begin();
	std::ofstream outputFile;


	while (true)
	{
		outputFile.open(outputFilePath, std::ios_base::app); //open file for append 
		
		if (messages.empty())
		{
			std::unique_lock<std::mutex> lk(this->_mtxOfMessages);
			this->_cv.wait(lk);
		}
		this->_mtxOfMessages.lock(); //lock the critical section
		while (!messages.empty())
		{
			currentMassege = messages.front();// get the first message
			messages.pop();
			
			for (it = users.begin(); it != users.end(); ++it)
			{
				outputFile << it->data() << ": " << currentMassege << std::endl; //print to file
			}
		}
		this->_mtxOfMessages.unlock(); //unlock the critical section
		
		outputFile.close();
	}
}