#include "Manager.h"

Manager::Manager()
{
	this->_menu = new Menu;
	this->_messagesSender = new MessagesSender;
}

Manager::~Manager()
{
	delete this->_menu;
	delete this->_messagesSender;
}
