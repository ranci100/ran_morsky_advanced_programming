#pragma once
#include <set>
#include <queue>
#include <string>
#include <iostream>
#include <chrono>
#include <mutex>
#include "InputException.h"
#include "MessagesSender.h"
#include "Menu.h"

class MessagesSender;
class Menu;
class Manager
{
public:
	Manager();
	~Manager();

	MessagesSender* _messagesSender;
	Menu* _menu;
	std::mutex _mtxOfUsers;
	
};