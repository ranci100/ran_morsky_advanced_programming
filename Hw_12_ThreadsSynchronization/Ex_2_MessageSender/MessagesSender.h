#pragma once
#include "Manager.h"
#include <set>
#include <queue>
#include <string>
#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include <mutex>
#include "InputException.h"

#define SIGNIN 1
#define SIGNOUT 2
#define CONNECTED_USERS 3
#define EXIT 4
#define TIME_TO_SLEEP_BETWEEN_GET_MESSAGES 60000


class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();
	
	//The function read from the data file all of the data messages
	void getMessagesFromFile(std::queue<std::string>& messages, std::string dataFilePath);
	//The function open the data file and the thread that reads from this file
	void callthreads(std::queue<std::string>& messages, std::string dataFilePath, std::string outputFilePath, std::set<std::string>& users);
	//the function print to the file all of the messages
	void sendMessagesToAllUsers(std::queue<std::string>& messages, std::set<std::string>& users, std::string outputFilePath);

private:
	std::mutex _mtxOfMessages;
	std::condition_variable _cv;
};




