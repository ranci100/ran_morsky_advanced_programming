#include "Manager.h"


#define DATA_FILE_PATH "data.txt"
#define OUTPUT_FILE_PATH "output.txt"

int main(void)
{
	std::set<std::string> users;
	std::queue<std::string> messages;
	int choose = 1;
	Manager manager;

	users.clear();
	manager._messagesSender->callthreads(messages, DATA_FILE_PATH,OUTPUT_FILE_PATH,users);
	
	while (choose >= SIGNIN && choose < EXIT)
	{
		manager._menu->printAndChooseOption(choose);

		//switch of options of system
		switch (choose)
		{
		case SIGNIN:
			manager._menu->addUser(users, manager._mtxOfUsers);
			
			break;
		case SIGNOUT:
			manager._menu->removeUser(users,manager._mtxOfUsers);
			break;
		case CONNECTED_USERS:
			manager._menu->printUsers(users);
			break;
		case EXIT:
			std::cout << "Good bye!" << std::endl;
			break;
		default:
			std::cout << "Please enter number from the option next time" << std::endl;
			choose = 1;
			break;
		}
	}
	return 0;
}