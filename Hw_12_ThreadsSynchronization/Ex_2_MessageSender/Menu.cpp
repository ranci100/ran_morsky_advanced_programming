#include "Menu.h"

Menu::Menu()
{
}

Menu::~Menu()
{
}

//The function add user to the set of users
void Menu::addUser(std::set<std::string>& users, std::mutex& mtxOfUsers)
{
	std::string newUser = "";
	std::cout << "Enter name of new user: ";
	std::cin >> newUser;

	if (users.count(newUser))
	{
		std::cout << "The user is already in our system" << std::endl;
	}
	else
	{
		mtxOfUsers.lock();//lock the critical section
		users.insert(newUser);
		mtxOfUsers.unlock();//unlock the critical section
	}
}

//The function remove user from the set of users
void Menu::removeUser(std::set<std::string>& users, std::mutex& mtxOfUsers)
{
	std::string userToRemove = "";
	std::cout << "Enter name of user to remove: ";
	std::cin >> userToRemove;
	if (users.count(userToRemove))
	{
		mtxOfUsers.lock();//lock the critical section
		users.erase(userToRemove);
		mtxOfUsers.unlock();//unlock the critical section
	}
	else
	{
		std::cout << "The user is not in our system" << std::endl;
	}
}

//The function print the options and get choice from user
int Menu::printAndChooseOption(int& choose)
{
	std::cout << "1	.Signin" << std::endl;
	std::cout << "2	.Signout" << std::endl;
	std::cout << "3	.Connected Users" << std::endl;
	std::cout << "4	.exit" << std::endl;

	std::cin >> choose;
	if (std::cin.fail()) //check input
	{
		std::cin.clear();
		std::cin.ignore();
		choose = EXIT + 1;
	}

	return 0;
}

//The function print the connected users in the system
void Menu::printUsers(std::set<std::string>& users)
{
	std::set<std::string>::iterator it;

	for (it = users.begin(); it != users.end(); ++it)
	{
		std::cout << '\t' << *it;
	}
	std::cout << std::endl;
}