#include "Customer.h"


#define NUMBER_OF_ITEMS 10

//the constructor init the name of the customer
Customer::Customer(std::string name)
{
	this->_name = name;
}

Customer::Customer()
{
}

//the destructor clear the list of items
Customer::~Customer()
{
	this->_items.clear();
}

//the function calculate the sum of the cost of the items for this customer
double Customer::totalSum() const 
{
	double sum = 0;
	std::set<Item>::iterator it = this->_items.begin();

	for (it ; it != this->_items.end(); ++it) //go over all of the items
	{
		sum += it->totalPrice();
	}

	return sum;
}

//the function add item to the customer's list of items
void Customer::addItem(Item newItem)
{
	if (this->_items.count(newItem))
	{
		Item itemToInsert(*this->_items.find(newItem));
		this->_items.erase(newItem);
		itemToInsert.incItem();
		this->_items.insert(itemToInsert);
	}
	else
	{
		this->_items.insert(newItem);
	}
}

// the function get name of item and removes this item from the list of items
void Customer::removeItem(Item itemToRemove)
{
	if (this->_items.count(itemToRemove))
	{
		if (this->_items.find(itemToRemove)->getNumOfItems() > 1)
		{
			Item itemToInsert(*this->_items.find(itemToRemove));
			this->_items.erase(itemToRemove);
			itemToInsert.decItem();
			this->_items.insert(itemToInsert);
		}
		else if (this->_items.find(itemToRemove)->getNumOfItems() == 1)
		{
			this->_items.erase(itemToRemove);
		}
	}
	
}