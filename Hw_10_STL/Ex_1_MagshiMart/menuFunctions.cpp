#include "menuFunctions.h"

//the function print the menu
void printMenu(int& choose)
{
	std::cout << "1. to sign as customer and buy items" << std::endl;
	std::cout << "2. to uptade existing customer's items" << std::endl;
	std::cout << "3. to print the customer who pays the most" << std::endl;
	std::cout << "4. to exit" << std::endl;
	choose = EXIT + 1;
	getChoicUser(choose, NEW_CUSTOMER, EXIT);
}

//the function adds items to customer's list of items 
void addItems(std::map<std::string, Customer>& abcCustomers,Item itemList[NUMBER_OF_ITEMS], std::string& newName)
{
	int chooseOption = NUMBER_OF_ITEMS+1;
	while (chooseOption != 0) //get items from user
	{
		std::cout << "What item would you like to buy? (0 - shopping is over) Input: " << std::endl;
		chooseOption = NUMBER_OF_ITEMS + 1;

		getChoicUser(chooseOption, 0, NUMBER_OF_ITEMS);

		if (chooseOption)
		{
			abcCustomers[newName].addItem(itemList[chooseOption - 1]);
		}
	}
}

//the function removes items from customer's list of items 
void removeItems(std::map<std::string, Customer>& abcCustomers, Item itemList[NUMBER_OF_ITEMS], std::string& name)
{
	int chooseOption = NUMBER_OF_ITEMS + 1;
	while (chooseOption != 0) //get items from user
	{
		std::cout << "What item would you like to remove? (0 - removing is over) Input: " << std::endl;
		chooseOption = NUMBER_OF_ITEMS + 1;

		getChoicUser(chooseOption, 0, NUMBER_OF_ITEMS);

		if (chooseOption)
		{
			abcCustomers[name].removeItem(itemList[chooseOption - 1]);
		}
	}
}

//the function print the most pay guy
void getMostPayGuy(std::map<std::string, Customer>& abcCustomers)
{
	std::map<std::string, Customer>::const_iterator it = abcCustomers.begin();
	std::string mostPayGuy = "";

	for (it; it != abcCustomers.end(); ++it) //go over all of the customers
	{
		if (abcCustomers[mostPayGuy].totalSum() <= abcCustomers[it->first].totalSum()) //check who pay more
		{
			mostPayGuy = it->first;
		}
	}
	//print answer
	std::cout << "most pay guy is: " << mostPayGuy << " with sum of: " << abcCustomers[mostPayGuy].totalSum() << std::endl;
}

//the function print the options to update customer
void printOptionToUpdateCustomer()
{
	std::cout << "1. Add items" << std::endl;
	std::cout << "2. Remove items " << std::endl;
	std::cout << "3. Back to menu" << std::endl;
}

//the function get choice of item from user
void getChoicUser(int &chooseOption,int minChoice, int maxChoice)
{
	while (chooseOption < minChoice || chooseOption > maxChoice)
	{
		std::cin >> chooseOption;
		try
		{
			if (std::cin.fail()) //check input
			{
				std::cin.clear();
				std::cin.ignore();
				throw InputException();
			}
			if (chooseOption < minChoice || chooseOption > maxChoice)
			{
				std::cout << "Please enter number from the options!" << std::endl;
			}
		}
		catch (const InputException & e)
		{
			std::cout << e.what() << std::endl;
			chooseOption = NUMBER_OF_ITEMS + 1;
		}	
	}
}