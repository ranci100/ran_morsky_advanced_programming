#include "Item.h"

//the constructor init all of the item variables 
Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
	this->_count = 1;
}

//the constructor init all of the item variables by the othe item
Item::Item(const Item& other)
{
	this->_name = other.getName();
	this->_serialNumber = other.getSerialNumber();
	this->_unitPrice = other.getUnitPrice();
	this->_count = other.getNumOfItems();
}

Item::~Item()
{
}

//the function calculate and return the price of the item\s 
double Item::totalPrice() const
{
	return this->_count*this->_unitPrice;
}

//the operator return bool value, true if this is bigger
bool Item::operator <(const Item& other) const
{
	return this->_unitPrice < other.getUnitPrice();
}

//the operator return bool value, true if this is smaller
bool Item::operator >(const Item& other) const
{
	return this->_unitPrice > other.getUnitPrice();
}

//the function check if this item is the same item like the other
bool Item::operator ==(const Item& other) const
{
	return other.getSerialNumber() == this->_serialNumber;
}

//the function print the info of this item
void Item::printInfo(int i) const
{
	std::cout << i << ". price: " << this->_unitPrice << this->_name << std::endl;
}

//the function return the serial number of this number
std::string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

//the function return the price of one of this item 
double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

//the function return the name of this item
std::string Item::getName() const
{
	return this->_name;
}

//the function return the number of this item
int Item::getNumOfItems() const
{
	return this->_count;
}

//the function add 1 to the count variable
void Item::incItem()
{
	this->_count++;
}

//the function sub 1 from the count variable
void Item::decItem()
{
	this->_count--;
}
