#pragma once
#include <iostream>
#include<map>
#include "Customer.h"
#include "inputException.h"

#define NUMBER_OF_ITEMS 10
#define ADD_ITEMS 1
#define REMOVE_ITEMS 2 
#define BACK_TO_MENU 3

#define NEW_CUSTOMER 1
#define UPDATE_CUSTOMER 2
#define RICHEST_CUSTOMER 3
#define EXIT 4

//the function print the menu
void printMenu(int &choose);
//the function adds items to customer's list of items 
void addItems(std::map<std::string, Customer>& abcCustomers, Item itemList[NUMBER_OF_ITEMS], std::string& newName);
//the function removes items from customer's list of items 
void removeItems(std::map<std::string, Customer>& abcCustomers, Item itemList[NUMBER_OF_ITEMS], std::string& name);
//the function print the most pay guy
void getMostPayGuy(std::map<std::string, Customer>& abcCustomers);
//the function print the options to update customer
void printOptionToUpdateCustomer();
//the function get choice of item from user
void getChoicUser(int& chooseOption, int minChoice, int maxChoice);