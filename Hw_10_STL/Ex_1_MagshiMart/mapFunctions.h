#pragma once
#include <string>
#include<map>
#include "Customer.h"

//the function check if the name exists in the map, if it's exist the function will return 1 else 0
int isExistInMap(std::string name, std::map<std::string, Customer>& customers);
