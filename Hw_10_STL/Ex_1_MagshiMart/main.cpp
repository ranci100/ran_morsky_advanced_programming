#include"Customer.h"
#include <map>
#include "mapFunctions.h"
#include "menuFunctions.h"


int main()
{
	int i = 1;
	int chooseOption = 0;
	std::string newName = "";
	std::map<std::string, Customer> abcCustomers;
	
	Item itemList[NUMBER_OF_ITEMS] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };

	std::cout << "Welcome to MagshiMart!" << std::endl;
	while (chooseOption != EXIT)
	{
		printMenu(chooseOption); //get the wanted option from user

		switch (chooseOption)
		{
		case NEW_CUSTOMER:
			std::cout << "what is your name?" << std::endl;
			std::cin >> newName;
			if (!isExistInMap(newName, abcCustomers)) //check if the customer exist
			{
				abcCustomers.insert(std::pair<std::string, Customer>(newName, Customer(newName))); //insert customer
				for (i; i <= NUMBER_OF_ITEMS; i++) //print options for numbers
				{
					itemList[i - 1].printInfo(i);
				}

				addItems(abcCustomers, itemList, newName); //adding items till get 0 from user
			}
			else
			{
				std::cout << "Customer has already exist :(" << std::endl << std::endl;
			}

			break;
		case UPDATE_CUSTOMER:
			std::cout << "what is the name to update?" << std::endl;
			std::cin >> newName;
			if (isExistInMap(newName, abcCustomers)) //check if the customer exist
			{
				printOptionToUpdateCustomer(); //print and get the wanted option from user
				chooseOption = 0;
				while (chooseOption < ADD_ITEMS || chooseOption > BACK_TO_MENU) //get choice from user
				{
					std::cin >> chooseOption;
				}

				switch (chooseOption)
				{
				case ADD_ITEMS:
					for (i; i <= NUMBER_OF_ITEMS; i++) //print options for numbers
					{
						itemList[i - 1].printInfo(i);
					}

					addItems(abcCustomers, itemList, newName);
					break;
				case REMOVE_ITEMS:
					removeItems(abcCustomers, itemList, newName);
					break;
				}
			}
			else
			{
				std::cout << "Customer don't exist :(" << std::endl << std::endl;
			}
			break;
		case RICHEST_CUSTOMER:
			getMostPayGuy(abcCustomers);
			break;
		}
	}

	return 0;
}