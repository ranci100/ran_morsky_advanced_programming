#pragma once
#include"Item.h"
#include <vector>
#include <iostream>
#include<set>
#include <forward_list>

class Customer
{
public:
	Customer(std::string);
	Customer();
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item newItem);//add item to the set
	void removeItem(Item);//remove item from the set

	//get and set functions

private:
	std::string _name;
	std::set<Item> _items;


};