#include "mapFunctions.h"

#define EXIST_IN_MAP 1
#define NOT_EXIST_IN_MAP 0

//the function check if the name exists in the map, if it's exist the function will return 1 else 0
int isExistInMap(std::string name, std::map<std::string, Customer>& customers)
{
	if (customers.count(name) > 0)
	{
		return EXIST_IN_MAP;
	}

	return NOT_EXIST_IN_MAP;
}