#pragma once

//the function calculate and return the area of pentagon
double CalPentagonArea(double rib);
//the function calculate and return the area of hexagon
double CalHexagonArea(double rib);
//the function calculate and return the paramieter of hexagon
double CalHexagonCircumference(double rib);
//the function calculate and return the paramieter of pentaagon
double CalPentagonCircumference(double rib);


