
#include "parallelogram.h"
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include <iostream>

#define DEGREES_IN_TRIANGLE 180

parallelogram::parallelogram(std::string col, std::string nam, int h, int w, double ang, double ang2):quadrilateral(col, nam, h, w) {
	 setAngle(ang, ang2);
}
void parallelogram::draw()
{
	try
	{
		std::cout << getName() << std::endl << getColor() << std::endl << "Height is " << getHeight() << std::endl << "Width is " << getWidth() << std::endl
			<< "Angles are: " << getAngle() << "," << getAngle2() << std::endl << "Area is " << CalArea(getWidth(), getHeight()) << std::endl;
	}
	catch (const shapeException & e)
	{
		std::cout << e.what() << std::endl;
	}
}

double parallelogram::CalArea(double w, double h) {
	if (w < 0 || h < 0)
	{
		throw shapeException();
	}
	return w*h;
}
void parallelogram::setAngle(double ang, double ang2) {
	if (ang + ang2 != DEGREES_IN_TRIANGLE || ang < 0 || ang > DEGREES_IN_TRIANGLE || ang2 < 0 || ang2 > DEGREES_IN_TRIANGLE)
	{
		throw shapeException();
	}
	angle = ang;
	angle2 = ang2;
}
double parallelogram::getAngle() {
	return angle;
}
double parallelogram::getAngle2() {
		return angle2;
	}
