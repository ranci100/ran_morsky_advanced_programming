#include "Pentagon.h"
#include "shapeexception.h"
#include "MathUtils.h"

//the constructor init the shape and the rib
Pentagon::Pentagon(std::string nam, std::string col, double rib) :Shape(col, nam)
{
	setRib(rib);
}

Pentagon::~Pentagon()
{

}

//the function draw the shape
void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "rib is " << getRib() << std::endl << "Circumference: " << CalPentagonCircumference(this->_rib) << "area is " << CalPentagonArea(this->_rib) << std::endl;;
}
//the function return the rib
double Pentagon::getRib() const
{
	return this->_rib;
}
//the function set the rib
void Pentagon::setRib(double rib)
{
	if (rib < 0)
	{
		throw InputException();
	}
	this->_rib = rib;
}