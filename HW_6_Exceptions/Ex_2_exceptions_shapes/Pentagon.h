#pragma once
#include "shape.h"
class Pentagon :public Shape
{
public:
	Pentagon(std::string nam, std::string col, double rib);
	~Pentagon();
	void draw() ;
	void setRib(double rib);
	//getters
	double getRib() const;


private:
	double _rib;

};

