#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include <string>
#include "Hexagon.h"
#include "Pentagon.h"


int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, rib = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam,col,rib);
	Hexagon hex(nam,col,rib);

	Shape* ptrcirc = &circ;
	Shape* ptrquad = &quad;
	Shape* ptrrec = &rec;
	Shape* ptrpara = &para;

	

	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; 
	std::string shapetype = "00";
	char x = 'y';
	while (x != 'x') {
		shapetype = "00";
		while (shapetype.size() > 1)
		{
			std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, P = pentagon, h = hexagon" << std::endl;

			std::cin >> shapetype;
			if (shapetype.size() > 1)
			{
				std::cout << "Warning � Don�t try to build more than one shape at once" << std::endl;
			}
		}
		try
		{

			switch (shapetype[0]) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				
				std::cin >> col >> nam >> rad;
				if (std::cin.fail())
				{
					throw InputException();
				}	
				
				circ.setColor(col);
				circ.setName(nam);
				try
				{
					circ.setRad(rad);
					ptrcirc->draw();
				}
				catch (const shapeException & e)
				{
					std::cout << e.what();
				}
				
				
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					throw InputException();
				}
				
				quad.setName(nam);
				quad.setColor(col);
				try
				{
					quad.setHeight(height);
					quad.setWidth(width);
					ptrquad->draw();
				}
				catch (const shapeException & e)
				{
					std::cout << e.what();
				}
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;

				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					throw InputException();
				}
				rec.setName(nam);
				rec.setColor(col);
				try
				{
					rec.setHeight(height);
					rec.setWidth(width);
					ptrrec->draw();
				}
				catch (const shapeException & e)
				{
					std::cout << e.what();
				}
				
				break;
			case 'h':
				std::cout << "enter name, color and rib" << std::endl;
				std::cin >> nam >> col >> rib;
				if (std::cin.fail())
				{
					throw InputException();
				}
				hex.setColor(col);
				hex.setName(nam);
				try
				{
					hex.setRib(rib);
					hex.draw();
				}
				catch (const InputException& e)
				{
					std::cout << e.what();
				}
				break;
			case 'P':
				std::cout << "enter name, color and rib" << std::endl;
				std::cin >> nam >> col >> rib;
				if (std::cin.fail())
				{
					throw InputException();
				}
				pent.setColor(col);
				pent.setName(nam);
				try
				{
					pent.setRib(rib);
					pent.draw();
				}
				catch (const InputException & e)
				{
					std::cout << e.what();
				}
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail())
				{
					throw InputException();
				}
				try 
				{
					para.setName(nam);
				}
				catch (const shapeException &e)
				{
					std::cout << e.what();
				}
				
				para.setColor(col);
				try
				{
					para.setHeight(height);
					para.setWidth(width);
				}
				catch (const shapeException & e)
				{
					std::cout << e.what();
				}
				try 
				{
					para.setAngle(ang, ang2);
					ptrpara->draw();
				}
				catch (const shapeException & e)
				{
					std::cout << e.what();
				}
				

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			getchar();
			std::cin >> x;
		}
		catch (const InputException & e)
		{
			std::cin.clear();
			std::cout << e.what();
		}
		catch (std::exception e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



	system("pause");
	return 0;

}