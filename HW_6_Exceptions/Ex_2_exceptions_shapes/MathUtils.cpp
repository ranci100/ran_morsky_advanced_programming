#include "MathUtils.h"
#include <math.h>

#define RIBS_IN_PENTAGON 5
#define RIBS_IN_HEXAGON 6

// Function to find area of pentagon 
double CalPentagonArea(double rib)
{
	// Formula to find area of pentagon
	return (sqrt(5 * (5 + 2 * (sqrt(5)))) * rib * rib) / 4;
}
//the function calculate and return the area of hexagon
double CalHexagonArea(double rib)
{
	return ((3 * sqrt(3) *(rib * rib)) / 2);
}
//the function calculate and return the paramieter of hexagon
double CalHexagonCircumference(double rib) 
{
	return rib * RIBS_IN_HEXAGON;
}
//the function calculate and return the paramieter of pentaagon
double CalPentagonCircumference(double rib)
{
	return rib * RIBS_IN_PENTAGON;
}