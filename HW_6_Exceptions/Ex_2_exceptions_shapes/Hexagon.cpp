#include "Hexagon.h"
#include "shapeexception.h"
#include "MathUtils.h"

//the constructor init the shape and the rib
Hexagon::Hexagon(std::string nam, std::string col, double rib):Shape(col, nam)
{
	setRib(rib);
}

Hexagon::~Hexagon()
{

}

//the function draw the shape
void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "rib is " << getRib() << std::endl << "Circumference: " << CalHexagonCircumference(this->_rib) << "area is " << CalHexagonArea(this->_rib) << std::endl;;
}
//the function return the rib
double Hexagon::getRib() const
{
	return this->_rib;
}

//the function set the rib
void Hexagon::setRib(double rib)
{
	if (rib < 0)
	{
		throw InputException();
	}
	this->_rib = rib;
}