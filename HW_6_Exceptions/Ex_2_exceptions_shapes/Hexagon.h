#pragma once
#include "shape.h"
class Hexagon:public Shape
{
public:
	Hexagon(std::string nam, std::string col, double rib);
	~Hexagon();
	void draw();
	void setRib(double rib);
	//getters
	double getRib() const;
	

private:
	double _rib;
};

