#include "shapeexception.h"
#include <iostream>

//the function return input error msg 
const char* InputException::what() const
{
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	return "This is input exception\n";
}