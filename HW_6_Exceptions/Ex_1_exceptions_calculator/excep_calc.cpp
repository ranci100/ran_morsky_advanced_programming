#include <iostream>

#define CAN_NOT_RETURN 8200

int add(int a, int b) {
	int result = 0;
	if (a + b == CAN_NOT_RETURN)
	{
		throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.");
	}
	else
	{
		result = a + b;
	}
	return result;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		try
		{
			sum = add(sum, a);
		}
		catch (std::string s)
		{
			std::cout << s;
		}
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	return exponent;
}

int main(void) {
	std::cout << pow(5, 5) << std::endl;
}