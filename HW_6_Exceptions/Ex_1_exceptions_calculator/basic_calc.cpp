#include <iostream>

#define CAN_NOT_RETURN 8200

int add(int a, int b,bool &isPossible) {
	int result = a + b;
	if (result == CAN_NOT_RETURN)
	{
		isPossible = false;
		return result + 1;
	}
	return result;
}

int  multiply(int a, int b, bool &isPossible) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a, isPossible);
	};
	if (!isPossible)
	{
		sum -= 1;
	}
	if (sum == CAN_NOT_RETURN)
	{
		isPossible = false;
		return sum + 1;
	}
	return sum;
}

int  pow(int a, int b, bool &isPossible) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a, isPossible);
	};
	if (!isPossible)
	{
		exponent -= 1;
	}
	if (exponent == CAN_NOT_RETURN)
	{
		isPossible = false;
		return exponent + 1;
	}
	return exponent;
}
/*
int main(void) {
	bool isPossible = true;
	int result = add(8100, 200, isPossible);
	if (isPossible)
	{
		std::cout << result << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.";
	}
}*/