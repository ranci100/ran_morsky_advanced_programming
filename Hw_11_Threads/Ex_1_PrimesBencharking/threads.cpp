#include "threads.h"
//The function print to the screen I Love Threads
void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

//The function create thread of that call the function I_Love_Threads
void call_I_Love_Threads()
{
	std::thread newThread(I_Love_Threads);
	newThread.join();
}

//The function get scale of numbers and ref to vector and put in only the prime numbers in the scale
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int i = 0;
	bool isPrime = true;

	primes.clear();

	//go over the nunmbers between begin and end
	for (; begin <+ end; begin++)
	{
		if (begin % EVEN == 0)
		{
			if (begin == 2) //two is the unusual case of prime
			{
				primes.push_back(begin);
			}
			begin++;
		}
		isPrime = true;
		for (i = EVEN+1 ; i <= begin/EVEN + 1 ;i += EVEN)//check if begin is prime
		{
			if(begin%i == 0)
			{
				isPrime = false;
				i = begin / EVEN + 1;
			}
		}
		if (begin % EVEN == 0)
		{
			isPrime = false;
		}
		if (isPrime)
		{
			primes.push_back(begin);
		}
	}
}
//The function get vector and print it 
void printVector(std::vector<int> primes)
{
	int i = 0;

	for (int i = 0; i < primes.size(); i++) 
	{
		std::cout << primes.at(i) << std::endl;
	}
}

//The function create a thread that call the function get primes and return vector of primes 
std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primes;
	
	std::thread newThread(getPrimes,begin, end, ref(primes));
	newThread.join();

	return primes;
}

//The function create N threads and print the prime numbers in the scale to file
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i = 0;
	bool isPrime = true;
	
	//go over the nunmbers between begin and end
	for (; begin < end; begin++)
	{
		if (begin % EVEN == 0)
		{
			if (begin == 2) //two is the unusual case of prime
			{
				file<< begin<<"\n";
			}
			begin++;
		}
		isPrime = true;
		for (i = EVEN + 1; i <= begin / EVEN + 1; i += EVEN)//check if begin is prime
		{
			if (begin % i == 0)
			{
				isPrime = false;
				i = begin / EVEN + 1;
			}
		}
		if (begin % EVEN == 0)
		{
			isPrime = false;
		}
		if (isPrime)
		{
			file << begin << "\n";
		}
	}
}

//The function create number of threads that use the function writePrimesToFile
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	int i = 0;
	std::vector<std::thread> threads;
	std::ofstream file;
	int scaleOfNumbers = (end - begin) / N;
	int currentEnd = begin + scaleOfNumbers;
	file.open(filePath);
	clock_t tStart = clock();

	for (i = 0; i < N; ++i)
	{
		threads.push_back(std::thread(writePrimesToFile,begin ,currentEnd ,ref(file)));
		begin += scaleOfNumbers;
		if (begin+scaleOfNumbers>end)
		{
			currentEnd = end;
		}
		else
		{
			currentEnd = begin + scaleOfNumbers;
		}
	}
	for (i = 0; i < N; i++)
	{
		if (threads[i].joinable())
		{
			threads[i].join();
		}
	}
	std::cout << "Time taken:  " << (double)(clock() - tStart) <<"s\n" << std::endl;
	file.close();
}

