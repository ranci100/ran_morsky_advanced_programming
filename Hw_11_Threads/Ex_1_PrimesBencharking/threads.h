#pragma once
#include <string>
#include <fstream>
#include <time.h>
#include <iostream>
#include<vector>
#include <thread>

#define EVEN 2

//The function print to the screen I Love Threads
void I_Love_Threads();
//The function print to the screen I Love Threads
void call_I_Love_Threads();
//The function get int vector and print the numbers in it
void printVector(std::vector<int> primes);
//The function get scale of numbers and ref to vector and put in only the prime numbers in the scale
void getPrimes(int begin, int end, std::vector<int>& primes);
//The function create a thread that call the function get primes and return vector of primes 
std::vector<int> callGetPrimes(int begin, int end);


void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);