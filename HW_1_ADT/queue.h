#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int * positiveNumbersArray;
	unsigned int arraySize;
	unsigned int lastInArray;
	unsigned int firstInArray;
	
} queue;

/*
	The function initializing the queue to the input size
	input: queue* q, unsigned int size
	output: none
*/
void initQueue(queue* q, unsigned int size);

/*
	The function turn all the values in the queue to Null
	input: queue* q
	output: none
*/
void cleanQueue(queue* q);

/*
	The function add value to the end of the queue
	input: queue* q, unsigned int newValue
	output: none
*/
void enqueue(queue* q, unsigned int newValue);

int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */