#include <iostream>
#include "queue.h"
using namespace std;

#define ARRAY_IS_EMPTY -1
 
/*
	The function initializing the queue to the input size
	input: queue* q, unsigned int size
	output: none
*/

void initQueue(queue* q, unsigned int size)
{
	unsigned int i = 0;

	q->positiveNumbersArray = new unsigned int[size];

	while (i < size)
	{
		q->positiveNumbersArray[i] = NULL;
		i++;
	}

	q->lastInArray = 0;
	q->firstInArray = 0;
	q->arraySize = size;
}

/*
	The function turn all the values in the queue to Null
	input: queue* q
	output: none
*/

void cleanQueue(queue* q)
{
	int i = 0;
	while (q->arraySize > i)
	{
		q->positiveNumbersArray[i] = NULL;
		i++;
	}
	q = NULL;
	delete q;
}
/*
	The function add value to the end of the queue
	input: queue* q, unsigned int newValue
	output: none
*/
void enqueue(queue* q, unsigned int newValue)
{
	cout << q->lastInArray << endl;
	cout << q->firstInArray << endl;
	
	if (q->lastInArray +1 == q->firstInArray  || q->firstInArray == 0 && q->lastInArray+1 == q->arraySize)
	{
		cout << "The array is full" << endl;//array is full
		cout << q->lastInArray << endl;
		cout << q->firstInArray << endl;
	}
	else
	{
		
		
		
			
		if (!(q->firstInArray == q->lastInArray && q->positiveNumbersArray[q->lastInArray] == NULL)) //check if is it the first number in the queue
		{
			q->lastInArray++;
			q->positiveNumbersArray[q->lastInArray] = newValue;
			if (q->lastInArray >= q->arraySize)
			{
		
				q->lastInArray = 0;
				q->positiveNumbersArray[q->lastInArray] = newValue;
				
			}
		}
		else
		{
			q->positiveNumbersArray[q->lastInArray] = newValue;
		}
		
		
		
		
	}
}

/*
	The function remove the first valeu
	input: queue* q
	output: the element in top of queue, or -1 if empty
*/

int dequeue(queue* q)
{
	int isEmpty = 0;
	
	
	cout << q->firstInArray << endl;
	cout << q->lastInArray << endl;
	q->positiveNumbersArray[q->firstInArray] = NULL;
	if (q->lastInArray == q->firstInArray )
	{
		isEmpty = ARRAY_IS_EMPTY;//array empty
	}
	else
	{
		q->firstInArray++;
		if (q->firstInArray >= q->arraySize)
		{
			q->firstInArray = 0;
		}

		
		
		isEmpty = q->positiveNumbersArray[q->firstInArray];
		

		
	}

	
	return isEmpty;
}