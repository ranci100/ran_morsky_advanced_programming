#include <iostream>
#include "linkedList.h"
#include "stack.h"

#define FALSE 0
#define TRUE 1


/* 
	The function insert new number into the list and return
	input:linkedList* topOfTheList,unsigned int number
	output: linkedList* newElement
*/
linkedList* insertNumberIntoList(linkedList* topOfTheList,unsigned int number)
{
	linkedList* newElement =  new linkedList();

	newElement->positiveNumber = number;	
	newElement->next = topOfTheList;
	
	
	return newElement;
}

/*
	The function delete part(number) from the linked list
	input:linkedList* topOfTheList
	output:linkedList* topOfTheList
*/
linkedList* deleteNumber(linkedList* topOfTheList)
{
	linkedList* currentlyNumber = topOfTheList;
		
	if (topOfTheList)
		topOfTheList = currentlyNumber->next;
		delete currentlyNumber;

	
	return topOfTheList;
}

/*
	The function free all of the allocated memory from the list
	input:linkedList* topOfTheList
	output:none
*/
void deleteList(linkedList* topOfTheList)
{

	linkedList* nextNumber = 0;

	while (topOfTheList != NULL)
	{
		nextNumber = topOfTheList->next;
		delete topOfTheList;
		topOfTheList = nextNumber;
	}
}

/*
	The function print the numbers in the linked list
	input:linkedList* top
	output:none
*/
void printList(linkedList* top)
{
	do
	{
		std::cout << top->positiveNumber << std::endl;
		top = top->next;
	} while (top != NULL);
}