#include <iostream>
#include "linkedList.h"
#include "stack.h"

/*
	The function gets pointer of stack struct and number to add and add the number to the linked list in the stack 
	input: stack* s, unsigned int element
	output: none
*/
void push(stack* s, unsigned int element)
{
	s->linkedStack = insertNumberIntoList(s->linkedStack, element);
}

/*
	The function gets pointer of stack struct ,return the latest number in the stack and delete the number from the stack or -1 if empty
	input: stack* s
	output: int isEmpty
*/
int pop(stack* s)
{
	int isEmpty = 0;
	if (s)
	{
		isEmpty = s->linkedStack->positiveNumber;
		s->linkedStack = deleteNumber(s->linkedStack);
	}
	else
	{
		isEmpty = -1;
	}

	return isEmpty;
}

/*
	The function init the stack
	input: stack* s
	output: none
*/
void initStack(stack* s)
{
	s->linkedStack = NULL;
}
	
/*
	The function free all of the allocated memory
	input: stack* s
	output: none
*/
void cleanStack(stack* s)
{
	deleteList(s->linkedStack);
}