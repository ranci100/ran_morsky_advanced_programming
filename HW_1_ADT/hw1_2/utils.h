#ifndef UTILS_H
#define UTILS_H

/*
	The function gets array pointer and the size of the array and reverse the array
	input: int* nums, unsigned int size
	output:none
*/
void reverse(int* nums, unsigned int size);

/*
	The function gets array pointer and the size of the array and reverse the array
	input: int* nums, unsigned int size
	output:int* nums
*/
int* reverse10();

#endif // UTILS_H