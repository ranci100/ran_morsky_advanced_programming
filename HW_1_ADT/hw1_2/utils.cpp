#include <iostream>
#include "linkedList.h"
#include "stack.h"

#define QUANTITY 10

/*
	The function gets array pointer and the size of the array and reverse the array 
	input: int* nums, unsigned int size
	output:none
*/
void reverse(int* nums, unsigned int size)
{
	stack stackToReverse;
	int i = 0;

	initStack(&stackToReverse);
	while (i < size)
	{
		push(&stackToReverse, nums[i]);
		i++;
	}

	i = 0;

	while (i < size)
	{
		nums[i] = pop(&stackToReverse);
		i++;
	}


}

/*
	The function gets array pointer and the size of the array and reverse the array
	input: int* nums, unsigned int size
	output:int* nums
*/
int* reverse10()
{
	int* nums = 0;
	int i = 0;
	nums = new int[QUANTITY];
	std::cout << "please enter 10 numbers" << std::endl;
	for (i = 0; i < QUANTITY; i++)
	{
		std::cin >> nums[i];
	}
	reverse(nums, QUANTITY);
	

		return nums;
}