#ifndef STACK_H
#define STACK_H


/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	linkedList* linkedStack;
} stack;

/*
	The function gets pointer of stack struct and number to add and add the number to the linked list in the stack
	input: stack* s, unsigned int element
	output: none
*/
void push(stack* s, unsigned int element);

/*
	The function gets pointer of stack struct ,return the latest number in the stack and delete the number from the stack or -1 if empty
	input: stack* s
	output: int isEmpty
*/
int pop(stack* s); 

/*
	The function init the stack
	input: stack* s
	output: none
*/
void initStack(stack* s);

/*
	The function free all of the allocated memory
	input: stack* s
	output: none
*/
void cleanStack(stack* s);

#endif // STACK_H