#ifndef LINKED_LIST_H
#define LINKED_LIST_H

typedef struct linkedList
{
	unsigned int positiveNumber;
	struct linkedList* next;
} linkedList;

linkedList* insertNumberIntoList(linkedList* topOfTheList, unsigned int number);
linkedList* deleteNumber(linkedList* topOfTheList);
void deleteList(linkedList* topOfTheList);
linkedList* recursiveReverse(linkedList* head);
void printList(linkedList* top);

#endif // STACK_H