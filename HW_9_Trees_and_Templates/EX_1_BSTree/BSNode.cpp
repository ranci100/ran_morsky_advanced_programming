#include "BSNode.h"
#include <string.h>
#include <iostream> 

//the constructor create the top of the tree
BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = 0;
	this->_right = 0;
	this->_count = 1;
}
//the constructor get other tree and create this tree according to the other tree
BSNode::BSNode(const BSNode& other)
{
	*this = other;
}

BSNode::~BSNode()
{
	delete this->_left;
	delete this->_right;
}
//the function insert new value to the right position
void BSNode::insert(std::string value)
{
	if (this->_data < value)
	{
		if (this->_right == NULL)
		{
			this->_right = new BSNode(value);
		}
		else
		{
			this->_right->insert(value);
		}
	}
	else if (this->_data > value)
	{
		if (this->_left == NULL)
		{
			this->_left = new BSNode(value);
		}
		else
		{
			this->_left->insert(value);
		}
	}
	else
	{
		this->_count++;
	}
}
//the operator do a deep copy from other tree
BSNode& BSNode::operator=(const BSNode& other)
{
	if (this != &other)
	{
		this->_data = other.getData();
		this->_left = other.getLeft();
		this->_right = other.getRight();
	}
	return *this;
}
//the function check if this is the leaf and return true or false
bool BSNode::isLeaf() const
{
	return this->_right == NULL && this->_left == NULL;
}
//the function return the data in this spesific 
std::string BSNode::getData() const
{
	return this->_data;
}
//the function return the left branch of the tree
BSNode* BSNode::getLeft() const
{
	return this->_left;
}
//the function return the right branch of the tree
BSNode* BSNode::getRight() const
{
	return this->_right;
}
//the function check and return if there if some value in the tree
bool BSNode::search(std::string val) const
{
	if (this == NULL)
	{
		return false; 
	}
	else if(this->_data == val)
	{
		return true;
	}
	else if (this->_data < val)
	{
		return this->_right->search(val);
	}
	else
	{
		return this->_left->search(val);
	}
}

//the function return the total hieght of the tree from this node in recursive way
int BSNode::getHeight() const
{
	if (this == NULL)
	{
		return 0;
	}
	else
	{
		/* compute the depth of each subtree */
		int left = this->_left->getHeight();
		int right = this->_right->getHeight();

		// use the larger one 
		if (left > right)
		{
			return(left + 1); //add 1 because we have also the first
		}
		else
		{
			return(right + 1);//add 1 because we have also the first
		}
	}
}

//the function return the distance between node to this node
int BSNode::getDepth(const BSNode& root) const 
{
	return getCurrNodeDistFromInputNode(&root);
}

//the function return the distance between node to this node in recursive way
int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const //auxiliary function for getDepth
{
	if (node->getData() == this->_data)
	{
		return 1;
	}
	else if (node->getLeft()->search(this->_data))
	{
		return getDepth(*node->getLeft()) + 1;
	}
	else if (node->getRight()->search(this->_data))
	{
		return getDepth(*node->getRight()) + 1;
	}
	return ROOT_DO_NOT_EXIST;
}

//the function print all of the roots in the tree in recursive way
void BSNode::printNodes() const
{
	if (this != NULL)
	{

		//go over the left side until get to the end
		this->_left->printNodes();
		//print node 
		std::cout << this->_data << " counter: " << this->_count << std::endl;
		//do the same thing but through the right side
		this->_right->printNodes();
	}
}