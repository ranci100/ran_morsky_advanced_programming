#include <fstream>
#include <string>
#include <windows.h>
#include <iostream>
#include "BSNode.h"
#define SIZE_OF_ARRAY 15

using std::cout;
using std::endl;

int main(void)
{
	int nums[SIZE_OF_ARRAY] = { 15,13,14,10,11,12,9,7,8,5,6,4,2,3,1 };
	char letters[SIZE_OF_ARRAY] = { 'c','b','a','d','f','e','g','h','o','v','y','i','p', 'z','t' };
	BSNode<int> intBs(nums[0]);
	BSNode<char> charBs(letters[0]);
	int i = 1;

	cout << nums[0] << " ";
	cout << letters[0] << " ";
	for (i; i < SIZE_OF_ARRAY; i++)
	{
		intBs.insert(nums[i]);
		cout << nums[i] << " ";
	}
	std::cout << endl;
	for (i = 1; i < SIZE_OF_ARRAY; i++)
	{
		charBs.insert(letters[i]);
		cout << letters[i] << " ";
	}
	std::cout << endl;

	intBs.printNodes();
	charBs.printNodes();

	
	return 0;
}

