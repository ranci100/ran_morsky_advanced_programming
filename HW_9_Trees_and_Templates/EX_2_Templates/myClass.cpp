#include "myClass.h"

MyClass::MyClass()
{
	this->_number = 0;
}
MyClass::MyClass(int number)
{
	this->_number = number;
}

MyClass::~MyClass()
{

}
//the function return the number
int MyClass::getNumber() const
{
	return this->_number;
}

bool MyClass::operator<(MyClass& other) const
{
	return this->_number < other.getNumber();
}
MyClass& MyClass::operator=(const MyClass& other) 
{
	if (this != &other)
	{
		this->_number = other.getNumber();
	}
	return  *this;
}
bool MyClass::operator==(MyClass& other) const
{
	return this->_number == other.getNumber();
}
//the function set number to the class MyClass
void MyClass::setNumber(int newNumber)
{
	this->_number = newNumber;
}

std::ostream& operator<<(std::ostream& out, MyClass& in)
{
	out << in.getNumber();
	return out;
}