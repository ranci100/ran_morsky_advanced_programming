#pragma once

#include <iostream>

class MyClass
{
public:
	MyClass();
	MyClass(int number);
	~MyClass();

	void setNumber(int newNumber);
	int getNumber() const;
	bool operator<(MyClass& other) const;
	MyClass& operator=(const MyClass& other);
	bool operator==(MyClass& other) const;
	friend std::ostream& operator<<(std::ostream& out, MyClass& in);
	

private:
	int _number;
};
