#pragma once

#include <iostream>

#define FIRST_IS_BIGGER -1
#define SECOND_IS_BIGGER 1

//the function compare between two templates and return 0 if equal
template <class Type>
int compare(Type a, Type b)
{
	if (a == b)
	{
		return 0;
	}
	else if (a < b)
	{
		return SECOND_IS_BIGGER;
	}
	return FIRST_IS_BIGGER;
}


//the function sort an array with bubble sort 
template <class Type>
void bubbleSort(Type array[], const int arraySize)
{
	unsigned int i = 0;
	unsigned int j = 0;
	Type temp;

	for (i = 0; i < arraySize; i++)
	{
		for (j = i + 1; j < arraySize; j++) // go from the last index check
		{
			if (array[j] < array[i]) // check if the new index is bigger then others
			{
				temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
		}
	}
}

//the function get array and print the elements in the array
template <class Type>
void printArray(Type array[], const int arraySize)
{
	unsigned int i = 0;
	for (i; i < arraySize; i++)
	{
		std::cout << array[i] << std::endl;
	}
}