#include "functions.h"
#include "myClass.h"
#include <iostream>



int main() {

	MyClass bug1;
	MyClass bug2;
	bug1.setNumber(15);
	bug2.setNumber(2);

	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<MyClass>(bug2, bug1) << std::endl;
	std::cout << compare<MyClass>(bug1, bug2) << std::endl;
	std::cout << compare<MyClass>(bug2, bug2) << std::endl;
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'b') << std::endl;
	std::cout << compare<char>('b', 'a') << std::endl;
	std::cout << compare<char>('a', 'a') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	MyClass numbers[arr_size] = { 0 };

	//set the array with some random values
	numbers[0].setNumber(50);
	numbers[1].setNumber(25);
	numbers[2].setNumber(42);
	numbers[3].setNumber(2);
	numbers[4].setNumber(10);

	
	bubbleSort<MyClass>(numbers, arr_size);
	for ( int i = 0; i < arr_size; i++ ) 
	{
		std::cout << numbers[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	
	std::cout << "correct print is sorted array" << std::endl;
	printArray<MyClass>(numbers, arr_size);
	std::cout << std::endl;
	
	system("pause");
	return 1;
}