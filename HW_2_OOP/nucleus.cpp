#include <iostream>
#include "nucleus.h"



#define LENGHT_OF_CODON 3

/*
	the function initialize the variables of Gene
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool is_on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_is_on_complementary_dna_strand = is_on_complementary_dna_strand;
}


/*
	the function return the variable _start
*/
unsigned int Gene::get_start() const
{
	return this->_start;
}

/*
	the function return the variable _end
*/
unsigned int Gene::get_end() const
{
	return this->_end;
}

/*
	the function return the variable _is_on_complementary_dna_strand
*/
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_is_on_complementary_dna_strand;
}


/*
	the function initialize the variables of Nucleus
*/
void Nucleus::init(const std::string dna_sequence)
{
	int i = 0;
	this->_DNA_strand = dna_sequence;
	std::string complementaryDNA = dna_sequence;

	while (i < dna_sequence.length())
	{
		if (complementaryDNA[i] == 'G')
		{
			complementaryDNA[i] = 'C';
		}
		else if (complementaryDNA[i] == 'C')
		{
			complementaryDNA[i] = 'G';
		}
		else if (complementaryDNA[i] == 'T')
		{
			complementaryDNA[i] = 'A';
		}
		else if (complementaryDNA[i] == 'A')
		{
			complementaryDNA[i] = 'T';
		}
		else
		{
			std::cerr << "One of the letters in the D.N.A is not G,C,A or T";
			_exit(1);
		}

		i++;
	}

	this->_complementary_DNA_strand = complementaryDNA;
}

/*
	the function return RNA transcript
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string rna_transcript = "";

	if (gene.is_on_complementary_dna_strand())
	{
		rna_transcript = this->_complementary_DNA_strand; //copy the R.N.A
	}
	else
	{
		rna_transcript = this->_DNA_strand; //copy the R.N.A
	}

	rna_transcript = change_T_to_U(rna_transcript.substr(gene.get_start(), gene.get_end() - gene.get_start() + 1));  //R.N.A change T to U
	
	return rna_transcript;
}

/*
	the function change all of the T letters to U letter
*/
std::string Nucleus::change_T_to_U(std::string rna) const
{
	unsigned int i = 0;
	while (i < rna.length())
	{
		if (rna[i] == 'T')
		{
			rna[i] = 'U';
		}

		i++;
	}

	return rna;
}

/*
	the function reverse the DNA
*/
std::string Nucleus::get_reversed_DNA_strand(std::string dnaStrand) const
{
	std::reverse(dnaStrand.begin(), dnaStrand.end());

	return dnaStrand;
}

/*
	the function count how many times the codon appears in the DNA
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int counter = 0;
	unsigned int pos = 0;

	do
	{
		pos = this->_DNA_strand.find(codon, pos);
		if (pos != std::string::npos)
		{
			counter++;
			pos += LENGHT_OF_CODON;
		}
	} while (pos != std::string::npos);

	return counter;
}
