#pragma once
#include "AminoAcid.h"
#include "protein.h"

/** A short class that represents a Node in an amino acid linked list */
class Mitochondrion
{
public:
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;


private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};