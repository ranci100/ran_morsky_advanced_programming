#pragma once
#include "AminoAcid.h"
#include "protein.h"
#include "Mitochondrion.h"
#include "nucleus.h"
#include "Ribosome.h"


/** A short class that represents a Node in an amino acid linked list */
class Cell
{
public:
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);


	// getters
	bool get_ATP();

	// setters


private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
};