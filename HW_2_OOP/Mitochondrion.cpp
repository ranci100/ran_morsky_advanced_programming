#include <iostream>
#include "Mitochondrion.h"
#include "protein.h"
#include "aminoAcid.h"

#define LENGHT_OF_PROTEIN 7
#define MIN_GLOCUSE_LEVELE_FOR_ATP 50

/*
	the function initialize the values of _glocuse_leves and _has glocuse_receptor
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

/*
	the function check if the protein is from the right type
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	unsigned int i = 0;

	if (protein.get_first()->get_data() == ALANINE)
	{
		if (protein.get_first()->get_next()->get_data() == LEUCINE)
		{
			if (protein.get_first()->get_next()->get_next()->get_data() == GLYCINE)
			{
				if (protein.get_first()->get_next()->get_next()->get_next()->get_data() == HISTIDINE)
				{
					if (protein.get_first()->get_next()->get_next()->get_next()->get_next()->get_data() == LEUCINE)
					{
						if (protein.get_first()->get_next()->get_next()->get_next()->get_next()->get_next()->get_data() == PHENYLALANINE)
						{
							if (protein.get_first()->get_next()->get_next()->get_next()->get_next()->get_next()->get_next()->get_data() == AMINO_CHAIN_END)
							{
								this->_has_glocuse_receptor = true;
							}
						}
					}
				}
			}
		}
	}
}




/*
	the function initialize the value of glocuse
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}


/*
	the function check id the mitochondrion can produce ATP and return True or False
*/
bool Mitochondrion::produceATP() const
{
	if (this->_has_glocuse_receptor == true && this->_glocuse_level >= MIN_GLOCUSE_LEVELE_FOR_ATP)
	{
		return true;
	}
	else
	{
		return false;
	}
}



