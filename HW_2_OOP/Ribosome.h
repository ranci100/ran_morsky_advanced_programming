#pragma once
#include <string>
#include "protein.h"

class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;

};