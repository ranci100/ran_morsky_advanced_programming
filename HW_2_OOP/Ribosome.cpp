#include <iostream>
#include "Ribosome.h"
#include "aminoAcid.h"
#include "protein.h"

#define MIN_NUCLEUS_TO_CREATE_PROTEIN 3
#define SECOND_NUCLEUS 1 
#define THERD_NUCLEUS 2

/*
	the function create a protein with a linked list
*/
Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	unsigned int currentPos = 0;
	std::string threeNucoleus = "";
	AminoAcid acid;
	Protein* proteinList = new Protein;
	bool checkFirst = true;
	
	proteinList->init();
	
	while (RNA_transcript.length() >= MIN_NUCLEUS_TO_CREATE_PROTEIN )
	{
		std::cout << "lenght of RNA: " << RNA_transcript.length() << std::endl;
		threeNucoleus = RNA_transcript.substr(0, MIN_NUCLEUS_TO_CREATE_PROTEIN);
		RNA_transcript = RNA_transcript.substr(MIN_NUCLEUS_TO_CREATE_PROTEIN, RNA_transcript.size());
		acid = get_amino_acid(threeNucoleus);  //get acid who appropriateb

		if (acid == UNKNOWN)
		{
			if (proteinList->get_first() != nullptr)
			{
				if (proteinList->get_first()->get_next() != nullptr)
				{
					proteinList->clear();
				}				
			}
			std::cerr << "Can't create protein" << std::endl;
			_exit(1);

		}
		else
		{
			proteinList->add(acid);	//add to the list
		}
	}
	return proteinList;
}