#include "cell.h"

#define MINIMUM_GLUCOSE_FOR_PROTEIN 50
#define AFTER_PRODUCE_ATP 100

/*
	the function initialize the values of all the variables of Cell
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
}

/*
	the function check if the mitochonndrion can create ATP and return true or false
*/
bool Cell::get_ATP()
{
	this->_mitochondrion.set_glucose(MINIMUM_GLUCOSE_FOR_PROTEIN);

	std::string RNA_transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene); //set RNA

	this->_mitochondrion.insert_glucose_receptor(*this->_ribosome.create_protein(RNA_transcript));
	if (this->_mitochondrion.produceATP())
	{
		this->_atp_units = AFTER_PRODUCE_ATP;
		return true;
	}
	else
	{
		return false;
	}
}