#pragma once
#include <string>


class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool is_on_complementary_dna_strand);


	// getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	// setters


private:
	unsigned int _start;
	unsigned int _end;
	bool _is_on_complementary_dna_strand;
};


class Nucleus
{
public:
	void init(const std::string dna_sequence);


	// getters
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand(std::string dnaStrand) const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
	// setters


private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;

	//methods
	std::string change_T_to_U(std::string rna) const;



};
